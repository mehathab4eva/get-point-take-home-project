import { useContext } from "react";
import { PersistorContext } from "../components/ApolloWrapper";

export const usePersistorContext = () => {
  return useContext(PersistorContext);
};
