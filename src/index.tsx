import React from "react";
import ReactDOM from "react-dom";
import { ApolloClient, ApolloProvider } from "@apollo/client";
import { persistCache, LocalStorageWrapper } from "apollo3-cache-persist";

import { App } from "./components/App";
import { cache } from "./cache";
import content from "./content";
import "./index.scss";

const uri = "https://gitlab.com/api/graphql";

persistCache({
  cache,
  storage: new LocalStorageWrapper(window.localStorage),
}).then(() => {
  const client = new ApolloClient({ uri, cache, connectToDevTools: true });

  ReactDOM.render(
    <ApolloProvider client={client}>
      <App content={content} />
    </ApolloProvider>,
    document.getElementById("root")
  );
});
