import { getPreviousStorageValue } from "./localStorageUtils";

export const getInitialPersistVarValue = <T>(
  storageName: string,
  initialValue: T
) => {
  const previousValue = getPreviousStorageValue(storageName);
  return previousValue === undefined ? initialValue : previousValue;
};
