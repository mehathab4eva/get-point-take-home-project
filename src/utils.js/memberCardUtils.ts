export const formatUserName = (userName: string) => `@${userName}`;
export const getAvatarText = (name: string | undefined) => name?.charAt?.(0);
