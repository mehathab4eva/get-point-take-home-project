export const getPreviousStorageValue = <T>(storageName: string) => {
  const previousValue = localStorage.getItem(storageName);
  if (previousValue !== null) {
    try {
      return JSON.parse(previousValue);
    } catch {
      // It wasn't JSON, assume a valid value
      return previousValue as unknown as T;
    }
  }
};
