import { AppContent } from "./types/content";

const content: AppContent = {
  header: "Project Search",
  description: "...powered by Gitlab's Graphql API.",
  searchContent: {
    inputId: "searchInputId",
    label: "Project",
    name: "projectName",
    placeholder: "Enter project name...",
    buttonId: "searchButtonId",
    buttonText: "Search",
  },

  footerContent: {
    nextButton: {
      text: "Next",
      id: "nextButtonId",
    },
    prevButton: {
      text: "Previous",
      id: "prevButtonId",
    },
  },
  projectListContent: {
    card: {
      title: { fallback: "Name unavailable", text: "Project", id: "name" },
      description: {
        text: "Description",
        fallback: "Description unavailable",
        id: "description",
      },
      action: { id: "learnMoreId", text: "Details" },
    },
    loadMore: { text: "Load More", id: "loadMoreId" },
    apiSupportText: {
      error: "Opps! An error occured. Please try your request again",
      success: "",
      noData: "No results found. Please try again.",
    },
  },
  projectDetailsContent: {
    title: { text: "Project Details", id: "title" },
    description: {
      text: "Description",
      fallback: "Description is unavailable",
      id: "description",
    },
    archived: {
      text: "Archived Status",
      fallback: "Archived status is  unavailable",
      id: "archived",
    },
    creation: {
      text: "Project creation date",
      fallback: "Unavailable",
      id: "createdAt",
    },
    action: { id: "closeButttonText", text: "Close" },
    users: {
      text: "Users",
      id: "projectMembers",
      fallback: "Members data unavailable",
    },
    url: {
      id: "webUrl",
      text: "URL",
      fallback: "Link is unavailable",
    },
    name: {
      text: "Project",
      fallback: "Name is unavailable",
      id: "projectName",
    },
    apiSupportText: {
      error: "Opps! An error occured. Please try your request again",
      success: "",
      noData: "No results found. Please try again.",
    },
  },
};

export default content;
