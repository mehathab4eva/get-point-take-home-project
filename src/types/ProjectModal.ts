export type ProjectFullPath = string;
export type IsShow = boolean;
export interface ProjectModal {
  isShow: IsShow;
  fullPath: ProjectFullPath;
}
