export interface SearchContent {
  inputId: string;
  label: string;
  name: string;
  placeholder: string;
  buttonId: string;
  buttonText: string;
}
export interface TextWithId {
  text: string;
  id: string;
}
export interface FieldTextWithFallback extends TextWithId {
  fallback?: string;
}

export interface FooterContent {
  nextButton: TextWithId;
  prevButton: TextWithId;
}
export interface Card<T> {
  title: T;
  description: T;
  action: TextWithId;
}
export interface ApiResponseStatusHandlingText {
  error: string;
  success: string;
  noData: string;
}
export interface ProjectDetailsContent<T> extends Card<T> {
  users: T;
  creation: T;
  archived: T;
  name: T;
  url: T;
  apiSupportText: ApiResponseStatusHandlingText;
}
export interface ProjectListContent<T> {
  card: T;
  loadMore: TextWithId;
  apiSupportText: ApiResponseStatusHandlingText;
}

export interface AppContent {
  header: string;
  description: string;
  searchContent: SearchContent;
  footerContent: FooterContent;
  projectDetailsContent: ProjectDetailsContent<FieldTextWithFallback>;
  projectListContent: ProjectListContent<Card<FieldTextWithFallback>>;
}
