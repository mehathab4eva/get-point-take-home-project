import { InMemoryCache, makeVar, ReactiveVar } from "@apollo/client";
import { ProjectModal } from "./types/ProjectModal";
import { SearchTerm } from "./types/SearchTerm";
import { getInitialPersistVarValue } from "./utils.js/reactiveVarUtils";

/** Reactive Variables  */

/** searchTerm - START */

export const SEARCH_TERM_STORAGE_NAME = "searchTerm";
const INITIAL_SEARCH_TERM: SearchTerm = null;

export const searchTermVar: ReactiveVar<SearchTerm> = makeVar<SearchTerm>(
  getInitialPersistVarValue(SEARCH_TERM_STORAGE_NAME, INITIAL_SEARCH_TERM)
);
/** searchTerm - END */

/** projectModal - START */

const INITIAL_PROJECT_MODAL: ProjectModal = {
  isShow: false,
  fullPath: "",
};

export const projectModalVar: ReactiveVar<ProjectModal> = makeVar<ProjectModal>(
  INITIAL_PROJECT_MODAL
);
/** projectModal - END */

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        searchTerm: {
          read() {
            return searchTermVar();
          },
        },
        projectModal: {
          read() {
            return projectModalVar();
          },
        },
        projects: {
          keyArgs: ["type"],

          merge(existing, incomming, { args }) {
            if (!args?.after && !!existing) return { ...existing };
            if (!args?.after) return { ...incomming };
            return {
              ...incomming,
              nodes: [...(existing?.nodes || []), ...(incomming?.nodes || [])],
            };
          },
        },
      },
    },
  },
});
