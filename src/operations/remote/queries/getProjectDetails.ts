import { gql } from "@apollo/client";

export const QUERY_PROJECT_DETAILS = gql`
  query ProjectCard($fullPath: ID!) {
    project(fullPath: $fullPath) {
      id
      name
      description
      archived
      createdAt
      webUrl
      projectMembers {
        nodes {
          user {
            id
            name
            publicEmail
            webUrl
            username
          }
        }
      }
    }
  }
`;
