import { gql } from "@apollo/client";

export const QUERY_PROJECT_LIST = gql`
  query ProjectList($search: String, $after: String) {
    projects(
      search: $search
      first: 20
      after: $after
      searchNamespaces: false
      sort: "name_asc"
    ) {
      nodes {
        id
        name
        description
        fullPath
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`;
