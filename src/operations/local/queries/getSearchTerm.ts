import { gql } from "@apollo/client";

export const GET_SEARCH_TERM = gql`
  query GetSearchTerm {
    searchTerm @client
  }
`;
