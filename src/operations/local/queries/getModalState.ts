import { gql } from "@apollo/client";

export const GET_MODAL_STATE = gql`
  query GetModalState {
    projectModal @client {
      isShow
      fullPath
    }
  }
`;
