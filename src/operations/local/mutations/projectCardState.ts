import { ReactiveVar } from "@apollo/client";
import { projectModalVar } from "../../../cache";
import { ProjectFullPath, ProjectModal } from "../../../types/ProjectModal";

const setProjectCard =
  (reactiveVar: ReactiveVar<ProjectModal>) =>
  (projectFullPath: ProjectFullPath) =>
    reactiveVar({ isShow: true, fullPath: projectFullPath });

const toggleProjectCard = (reactiveVar: ReactiveVar<ProjectModal>) => () => {
  const { isShow, ...rest } = reactiveVar();
  return reactiveVar({ isShow: !isShow, ...rest });
};

export const projectCardMutations = {
  setProjectCard: setProjectCard(projectModalVar),
  toggleProjectCard: toggleProjectCard(projectModalVar),
};
