import { ReactiveVar } from "@apollo/client";
import { searchTermVar, SEARCH_TERM_STORAGE_NAME } from "../../../cache";
import { SearchTerm } from "../../../types/SearchTerm";

const setSearchTerm =
  (reactiveVar: ReactiveVar<SearchTerm>) => (newSearchTerm: SearchTerm) => {
    if (newSearchTerm === null) {
      localStorage.removeItem(SEARCH_TERM_STORAGE_NAME);
    } else {
      localStorage.setItem(SEARCH_TERM_STORAGE_NAME, newSearchTerm);
    }
    return reactiveVar(newSearchTerm);
  };

export const searchTermMutation = {
  setSearchTerm: setSearchTerm(searchTermVar),
};
