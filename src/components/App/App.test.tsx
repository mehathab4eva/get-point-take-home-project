import React from "react";
import { render, screen } from "@testing-library/react";
import { App } from "./App";
import content from "../../content";
import { AppContent } from "../../types/content";

test("renders learn react link", () => {
  render(<App content={content as AppContent} />);
  const linkElement = screen.getByText(/Project Search/i);
  expect(linkElement).toBeInTheDocument();
});
