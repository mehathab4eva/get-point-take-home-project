import { useReactiveVar } from "@apollo/client";
import { useEffect, useState } from "react";
import cx from "classnames";
import { TextSearch } from "../commons/TextSearch/TextSearch";
import { searchTermVar } from "../../cache";
import { searchTermMutation } from "../../operations/local/mutations";
import ProjectList from "../ProjectList";
import { AppContent } from "../../types/content";
import ProjectDetails from "../ProjectDetails";
import styles from "./App.module.scss";

interface OwnProps {
  content: AppContent;
}

export function App({ content }: OwnProps) {
  const [input, setInput] = useState("");
  const searchTerm = useReactiveVar(searchTermVar);
  const isSearchTermEstablished = searchTerm !== null;

  useEffect(() => {
    if (searchTerm) setInput(searchTerm);
  }, [searchTerm]);

  return (
    <main
      className={cx(styles.container, {
        [styles.postSearch]: isSearchTermEstablished,
      })}
    >
      <header className={styles.headerContainer}>
        <section className={styles.titleContainer}>
          <h1 className={styles.title}>{content.header}</h1>
          <p className={styles.titleDescription}>{content.description}</p>
        </section>

        <TextSearch
          value={input}
          className={styles.search}
          formId='projectSearchForm'
          isDisabled={searchTerm === input}
          placeholder={content?.searchContent?.placeholder}
          onChange={(e) => {
            setInput(e.currentTarget.value);
          }}
          onSubmit={() => {
            searchTermMutation.setSearchTerm(input);
          }}
        />
      </header>
      {isSearchTermEstablished && (
        <section className={styles.searchListContainer}>
          <ProjectList
            className={styles.searchList}
            searchTerm={searchTerm}
            content={content.projectListContent}
          />
        </section>
      )}
      <ProjectDetails content={content.projectDetailsContent} />
    </main>
  );
}
