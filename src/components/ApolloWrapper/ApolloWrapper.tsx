import {
  ApolloClient,
  ApolloProvider,
  NormalizedCacheObject,
} from "@apollo/client";
import {
  createContext,
  ReactNode,
  useCallback,
  useEffect,
  useState,
} from "react";
import { CachePersistor, LocalStorageWrapper } from "apollo3-cache-persist";

import { cache } from "../../cache";

interface OwnProps {
  children: ReactNode;
}
export const PersistorContext = createContext(
  {} as CachePersistor<NormalizedCacheObject> | undefined
);
export function ApolloWrapper({ children }: OwnProps) {
  const [client, setClient] = useState<ApolloClient<NormalizedCacheObject>>();
  const [persistor, setPersistor] =
    useState<CachePersistor<NormalizedCacheObject>>();

  useEffect(() => {
    async function init() {
      const newCache = cache;
      let newPersistor = new CachePersistor({
        cache: newCache,
        storage: new LocalStorageWrapper(window.localStorage),
        debug: true,
        // trigger: "write",
      });
      await newPersistor.restore();
      setPersistor(newPersistor);
      const uri = "https://gitlab.com/api/graphql";

      setClient(
        new ApolloClient({ uri, cache: newCache, connectToDevTools: true })
      );
    }

    init().catch(console.error);
  }, []);

  const clearCache = useCallback(() => {
    if (!persistor) {
      return;
    }
    persistor.purge();
  }, [persistor]);

  const reload = useCallback(() => {
    window.location.reload();
  }, []);

  if (!client) {
    return <h2>Initializing app...</h2>;
  }

  return (
    <PersistorContext.Provider value={persistor}>
      <ApolloProvider client={client}>{children}</ApolloProvider>
    </PersistorContext.Provider>
  );
}
