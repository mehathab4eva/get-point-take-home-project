import { Card, Skeleton } from "antd";
import { ReactNode } from "react";
import styles from "./ShimmerCards.module.scss";
interface ShimmerCardsProps {
  count: number;
}

const drawShimmerCards = (
  count: number,
  aggShimmerCards = [] as ReactNode[]
): ReactNode[] => {
  if (count === 0) return aggShimmerCards;
  return drawShimmerCards(count - 1, [
    ...aggShimmerCards,
    <Card
      className={styles.shimmerCard}
      key={count}
      title={<Skeleton.Avatar active size='large' />}
      extra={<Skeleton.Button active size='large' />}
    >
      <Skeleton round title active paragraph={{ rows: 3, width: 90 }} />
    </Card>,
  ]);
};

export const ShimmerCards = ({ count }: ShimmerCardsProps) => {
  return <>{drawShimmerCards(count)}</>;
};
