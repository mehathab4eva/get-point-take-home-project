import cx from "classnames";
import { Project, ProjectListQuery } from "../../generated/graphql";
import {
  Card,
  FieldTextWithFallback,
  ProjectListContent,
} from "../../types/content";
// import { Row, Col, Avatar, Typography, Button } from "antd";

import { ProjectFullPath } from "../../types/ProjectModal";
import ProjectCard from "../Card";
import styles from "./ProjectList.module.scss";
import { ShimmerCards } from "./ShimmerCards";

interface OwnProps {
  content: ProjectListContent<Card<FieldTextWithFallback>>;
  data: ProjectListQuery;
  currentProjectFullPath: ProjectFullPath;
  onProjectSelect: (fullPath: ProjectFullPath) => void;
  className?: string;
  loading: boolean;
  aNewSearchLoading: boolean;
}
export const ProjectList = ({
  data,
  content,
  onProjectSelect,
  currentProjectFullPath,
  className = "",
  loading,
  aNewSearchLoading,
}: OwnProps) => {
  const handleProjectSelect = (fullPath: ProjectFullPath) => () => {
    onProjectSelect(fullPath);
  };
  return (
    <section className={cx(styles.container, { [className]: !!className })}>
      {!aNewSearchLoading &&
        data?.projects?.nodes?.map?.((eachProject, i) => {
          const { id, name, description, fullPath } = eachProject as Project;

          return (
            <ProjectCard
              avatarText={i + 1}
              content={content.card}
              handleAction={handleProjectSelect(fullPath)}
              title={name}
              description={description}
              key={id}
            />
          );
        })}
      {loading && <ShimmerCards count={20} />}
    </section>
  );
};
