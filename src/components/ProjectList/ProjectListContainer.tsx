import { ApolloError, useReactiveVar } from "@apollo/client";
import { useEffect } from "react";
import { Button, Empty, Result, message } from "antd";
import { projectModalVar } from "../../cache";
import { ProjectList } from ".";
import {
  ProjectListQuery,
  useProjectListLazyQuery,
} from "../../generated/graphql";
import { projectCardMutations } from "../../operations/local/mutations";
import {
  Card,
  FieldTextWithFallback,
  ProjectListContent,
} from "../../types/content";
import { SearchTerm } from "../../types/SearchTerm";
import styles from "./ProjectListContainer.module.scss";

interface OwnProps {
  searchTerm: SearchTerm;
  content: ProjectListContent<Card<FieldTextWithFallback>>;
  className?: string;
}

export const ProjectListContainer = ({
  searchTerm,
  content,
  className,
}: OwnProps) => {
  const onCompletedCallback = (data: ProjectListQuery) => {
    const newDataLength = data?.projects?.nodes?.length;

    message.success(`Total fetched projects: ${newDataLength}`);
  };

  const onErrorCallback = (error: ApolloError) => {
    message.error(
      "Opps! An error occured while getting the Project search results."
    );
  };

  const [getProjectList, { data, loading, fetchMore, error, variables }] =
    useProjectListLazyQuery({
      notifyOnNetworkStatusChange: true,
      fetchPolicy: "cache-and-network",
      onCompleted: onCompletedCallback,
      onError: onErrorCallback,
    });

  const { fullPath } = useReactiveVar(projectModalVar);
  const aNewSearch = searchTerm !== variables?.search;
  const aNewSearchLoading = loading && aNewSearch;

  const onFetchMore = () => {
    fetchMore({
      variables: { after: data?.projects?.pageInfo?.endCursor || "" },
    });
  };

  useEffect(() => {
    if (searchTerm !== null && aNewSearch)
      getProjectList({ variables: { search: searchTerm, after: "" } });
  }, [getProjectList, searchTerm, aNewSearch]);

  if (!data?.projects?.nodes?.length && error && !loading)
    return (
      <div className={className}>
        <Result status='error' subTitle={content.apiSupportText.error} />
      </div>
    );
  if (!data?.projects?.nodes?.length)
    return (
      <div className={className}>
        <Empty description={content?.apiSupportText?.noData} />
      </div>
    );

  return (
    <>
      {data && (
        <ProjectList
          data={data}
          content={content}
          className={className}
          onProjectSelect={projectCardMutations.setProjectCard}
          currentProjectFullPath={fullPath}
          loading={loading}
          aNewSearchLoading={aNewSearchLoading}
        />
      )}

      <Button
        type='dashed'
        size='large'
        className={styles.loadMoreButton}
        disabled={loading || !data?.projects?.pageInfo?.hasNextPage}
        onClick={onFetchMore}
        id={content.loadMore.id}
      >
        {content.loadMore.text}
      </Button>
    </>
  );
};
