import {
  Descriptions as DescriptionsAnt,
  DescriptionsProps as DescriptionsPropsAnt,
} from "antd";
import { FieldTextWithFallback } from "../../../types/content";
import { Item } from "./Item";

export interface DescriptionsProps extends DescriptionsPropsAnt {
  data: any;
  schema: FieldTextWithFallback[];
}
export const Descriptions = ({
  data,
  schema,
  ...restProps
}: DescriptionsProps) => {
  return (
    <DescriptionsAnt {...restProps}>
      {schema?.length > 0 &&
        schema?.map?.((item) => {
          return (
            <Item
              key={item?.id}
              label={item?.text}
              text={data?.[item?.id]}
              fallback={item?.fallback}
            />
          );
        })}
    </DescriptionsAnt>
  );
};
