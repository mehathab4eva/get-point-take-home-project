import { Typography, Descriptions } from "antd";
import { ReactNode } from "react";

interface OwnProps {
  label: ReactNode;
  text: any;
  fallback?: ReactNode;
}
export const Item = ({ label, text, fallback, ...restProps }: OwnProps) => {
  return (
    <Descriptions.Item
      span={4}
      label={<Typography.Text strong>{label}</Typography.Text>}
      {...restProps}
    >
      <Typography.Text>{text || fallback}</Typography.Text>
    </Descriptions.Item>
  );
};
