import { ReactEventHandler } from "react";
import { Input, InputProps } from "antd";

interface OwnProps extends InputProps {
  // onSubmit?: ReactEventHandler<HTMLFormElement>;
  onChange: ReactEventHandler<HTMLInputElement>;
  isDisabled?: boolean;
  formId: string;
  value: string;
}

export const TextSearch = ({
  onSubmit,
  onChange,
  isDisabled,
  formId,
  value,
  ...restProps
}: OwnProps) => {
  const handleOnSubmit = (e: any) => {
    if (!isDisabled && onSubmit) onSubmit(e);
  };

  return (
    <Input.Search
      allowClear
      enterButton='Search'
      size='large'
      value={value}
      onChange={onChange}
      onSearch={handleOnSubmit}
      onPressEnter={handleOnSubmit}
      // disabled={isDisabled}
      {...restProps}
    />
  );
};
