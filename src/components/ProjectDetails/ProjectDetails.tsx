import { ApolloError } from "@apollo/client";
import {
  Drawer,
  Button,
  Descriptions,
  Divider,
  Empty,
  Skeleton,
  Result,
} from "antd";
import { useEffect, useState } from "react";

import { ProjectCardQuery } from "../../generated/graphql";
import {
  FieldTextWithFallback,
  ProjectDetailsContent,
} from "../../types/content";
import { getAvatarText } from "../../utils.js/memberCardUtils";
import Card from "../Card";
import { ShimmerCards } from "../ProjectList/ShimmerCards";
import styles from "./ProjectDetails.module.scss";

interface OwnProps {
  loading: boolean;
  data: ProjectCardQuery;
  isShow: boolean;
  onClose: () => void;
  content: ProjectDetailsContent<FieldTextWithFallback>;
  error: ApolloError | undefined | boolean;
}
const TABLET_DEVICE_SCREEN_WIDTH = 768;

export const ProjectDetails = ({
  data,
  loading,
  isShow,
  onClose,
  content,
  error,
}: OwnProps) => {
  const { archived, createdAt, description, name, projectMembers, webUrl } =
    data?.project || {};
  const projectMemeberList = projectMembers?.nodes || [];
  const [isSmallScreen, setsetIsSmallScreen] = useState<boolean>(false);

  useEffect(() => {
    setsetIsSmallScreen(window.innerWidth < TABLET_DEVICE_SCREEN_WIDTH);
  }, [isShow]);
  return (
    <Drawer
      className={styles.projectDetails}
      title={<h2 className={styles.title}>Project Details</h2>}
      placement='right'
      width={isSmallScreen ? "95vw" : 736}
      onClose={onClose}
      visible={isShow}
      extra={
        !isSmallScreen && (
          <Button type='primary' onClick={onClose}>
            OK
          </Button>
        )
      }
    >
      {error ? (
        <Result status='error' subTitle={content.apiSupportText.error} />
      ) : (
        <>
          <Descriptions
            className={styles.details}
            title={
              loading ? (
                <Skeleton title active />
              ) : (
                <h3 className={styles.title}>{name}</h3>
              )
            }
            bordered
            column={1}
            layout={isSmallScreen ? "vertical" : "horizontal"}
            size={isSmallScreen ? "small" : "middle"}
            extra={
              loading ? (
                <Skeleton.Button active />
              ) : (
                <Button target='_blank' type='link' href={webUrl || "#"}>
                  View in Gitlab
                </Button>
              )
            }
          >
            <Descriptions.Item
              label={<p className={styles.label}>{content.description.text}</p>}
            >
              {loading ? (
                <Skeleton.Button size='large' active />
              ) : (
                <p className={styles.value}>
                  {description || content.description.fallback}
                </p>
              )}
            </Descriptions.Item>
            <Descriptions.Item
              label={<p className={styles.label}>{content.creation.text}</p>}
            >
              {loading ? (
                <Skeleton.Button size='large' active />
              ) : (
                <p className={styles.value}>
                  {new Date(createdAt).toDateString()}
                </p>
              )}
            </Descriptions.Item>
            <Descriptions.Item
              label={<p className={styles.label}>{content.archived.text}</p>}
            >
              {loading ? (
                <Skeleton.Button size='large' active />
              ) : (
                <p className={styles.value}>{archived ? "Yes" : "No"}</p>
              )}
            </Descriptions.Item>
            <Descriptions.Item
              span={isSmallScreen ? 1 : 3}
              label={<p className={styles.label}>{content.url.text}</p>}
            >
              {loading ? (
                <Skeleton.Button size='large' active />
              ) : webUrl ? (
                <Button
                  className={styles.value}
                  target='_blank'
                  type='link'
                  href={webUrl || "#"}
                >
                  {webUrl}
                </Button>
              ) : (
                content.url.fallback
              )}
            </Descriptions.Item>
          </Descriptions>

          <Divider orientation='left' plain style={{ marginTop: "4rem" }}>
            <h4 className={styles.membersTitle}>Project Members</h4>
          </Divider>

          <section className={styles.memberList}>
            {projectMemeberList?.map?.((eachMember, i) => {
              const { username, name, webUrl, publicEmail } =
                eachMember?.user || {};
              return (
                <Card
                  className={styles.memberCard}
                  avatarText={getAvatarText(name)}
                  extra={
                    <a
                      className={styles.userName}
                      target='_blank'
                      href={webUrl}
                      rel='noreferrer'
                    >
                      {`@${username}`}
                    </a>
                  }
                >
                  <h5 className={styles.name}>{name}</h5>
                  <p className={styles.email}>
                    {publicEmail || (
                      <span className={styles.empty}>Not available</span>
                    )}
                  </p>
                </Card>
              );
            })}
            {loading && <ShimmerCards count={2} />}
            {!projectMemeberList.length && !loading && (
              <Empty description={content.users.fallback} />
            )}
          </section>
        </>
      )}
      <Button onClick={onClose} className={styles.closeButton} type='link'>
        Close
      </Button>
    </Drawer>
  );
};
