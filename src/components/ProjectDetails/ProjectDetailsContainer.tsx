import { useReactiveVar } from "@apollo/client";
import { useEffect } from "react";
import { ProjectDetails } from ".";
import { projectModalVar } from "../../cache";
// import { message } from "antd";
import {
  ProjectCardQuery,
  useProjectCardLazyQuery,
} from "../../generated/graphql";
import { projectCardMutations } from "../../operations/local/mutations";
import {
  FieldTextWithFallback,
  ProjectDetailsContent,
} from "../../types/content";

interface OwnProps {
  content: ProjectDetailsContent<FieldTextWithFallback>;
}
export const ProjectCardContainer = ({ content }: OwnProps) => {
  const { fullPath, isShow } = useReactiveVar(projectModalVar);
  // const onCompletedCallback = (data: ProjectCardQuery) => {
  //   message.success("Operation: Product Details is successful!");
  // };

  // const onErrorCallback = (error: ApolloError) => {
  //   message.error("Opps! An error occured while getting the Project Details.");
  // };

  const [getProjectDetails, { data, loading, error }] = useProjectCardLazyQuery(
    {
      notifyOnNetworkStatusChange: true,
      // onError: onErrorCallback,
      // onCompleted: onCompletedCallback,
    }
  );

  useEffect(() => {
    if (fullPath) {
      getProjectDetails({
        variables: {
          fullPath,
        },
      });
    }
  }, [fullPath, getProjectDetails]);
  return (
    <ProjectDetails
      loading={loading}
      data={data as ProjectCardQuery}
      isShow={isShow}
      onClose={projectCardMutations.toggleProjectCard}
      content={content}
      error={error}
    />
  );
};
