import { Card as CardComponent, Avatar, Button } from "antd";
import cx from "classnames";
import { ReactNode } from "react";
import {
  Card as CardContentType,
  FieldTextWithFallback,
} from "../../types/content";
import styles from "./Card.module.scss";

interface OwnProps {
  avatarText: number | string | undefined;
  className?: string;
  content?: CardContentType<FieldTextWithFallback>;
  handleAction?: () => void;
  title?: string | null;
  description?: string | null;
  extra?: ReactNode;
  children?: ReactNode;
}
export const Card = ({
  avatarText,
  className = "",
  content,
  handleAction,
  title,
  description,
  extra,
  children,
}: OwnProps) => {
  return (
    <CardComponent
      className={cx(styles.card, { [className]: !!className })}
      title={
        <Avatar className={styles.avatar} size='large'>
          {avatarText}
        </Avatar>
      }
      extra={
        extra || (
          <Button
            className={styles.actionButton}
            onClick={handleAction}
            type='primary'
          >
            {content?.action?.text}
          </Button>
        )
      }
    >
      {children || (
        <>
          <h3 className={styles.title} title={content?.title?.text}>
            {title || content?.title.fallback}
          </h3>
          <p className={styles.description}>
            {description || (
              <span className={styles.empty}>
                {content?.description?.fallback}
              </span>
            )}
          </p>
        </>
      )}
    </CardComponent>
  );
};
