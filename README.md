# get-point-take-home-project

This project uses typescript, apollo-client, graphql, scss. The stage management is handled by Apollo through [Reactive Vars](https://www.apollographql.com/docs/react/local-state/reactive-variables/).

It calls the gitlab's graphql server on [GraphQL API](https://gitlab.com/api/graphql)

Currently the below quries are used in the project.

1. Projects (Read Only) -This will give you list of all the projects that matchest the search criteria.

2. Project (Read Only) - This will fetch the project details of a project that matches the project's fullpath.

For more information please refer [Official Gitlab Graphql Api Documentation](https://docs.gitlab.com/ee/api/graphql/)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn codegen`

If add new graphql qureies and need to generate its types and suporting react hooks,run this command.

Please note that the generated code has some duplicatd Enums generated during the codegen. I suggect you to manually fix them deleting the lowercase (deprecated) Enum values in the Enum declaration.

For more injormation on Codegen, please refer, [Build a GraphQL + React app with TypeScript](https://blog.logrocket.com/build-graphql-react-app-typescript/).

## Getting started

```
yarn install && yarn start
```

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mehathab4eva/get-point-take-home-project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://gitlab.com/mehathab4eva/get-point-take-home-project/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:8a1e823f0cdf64e02ff4ffbe835dd31f?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
